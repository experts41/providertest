package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("A request to create RTF")
    request {
        method 'GET'
        url '/assets'
        headers {
            header('Content-Type', 'application/json')
            header('callingService', '87878787')
            header('transactionId', 'CAS.CAM1351450074')
            header('userId', 'x306486')
            header('userLocation', 'HDQ')
        }
        body(
                activateDate: "2023-12-31T23:59:59.999-05:00",
                bookings: [
                        [
                                createDate: "2023-01-01T23:59:59.999-05:00",
                                id: "{{PNR}}",
                                sourceOfSale: [
                                        originatorTypeCode: "A",
                                        reservationSystemCode: "1A"
                                ],
                                type: "air"
                        ]
                ],
                corporations: [
                        [
                                arcNumber: "2212333",
                                company: [
                                        id: "{{CompID}}",
                                        name: "Southwest Airlines"
                                ],
                                osi: "some value",
                                pcc: "AMSX1234P",
                                tourCode: "403SH"
                        ]
                ],
                coupons: [
                        [
                                arrivalAirportCode: "DAL",
                                departureAirportCode: "HOU",
                                departureDate: "2023-03-01T23:59:59.999-05:00",
                                fareBasisCode: "FZMUHNR",
                                fareFamily: "BUS",
                                flightNumber: "1234",
                                marketingCarrierCode: "WN",
                                number: 1
                        ]
                ],
                expirationDate: "2040-12-31T23:59:59.999-05:00",
                issuanceContext: [
                        isReissued: false,
                        location: "RrzHLn1ud7",
                        reason: "Ticket Refund",
                        reasonDetails: "Additional details",
                        sendFulfillmentEmail: false
                ],
                issuedBalances: [
                        [
                                amount: 1000,
                                expirationDate: "2024-01-01T23:59:59.999-05:00",
                                type: "refundable",
                                uom: "USD"
                        ],
                        [
                                amount: 1000,
                                expirationDate: "2024-01-01T23:59:59.999-05:00",
                                type: "nonRefundable",
                                uom: "USD"
                        ],
                        [
                                amount: 1000,
                                expirationDate: "2024-01-01T23:59:59.999-05:00",
                                type: "transferable",
                                uom: "USD"
                        ],
                        [
                                amount: 1000,
                                expirationDate: "2024-01-01T23:59:59.999-05:00",
                                type: "nonTransferable",
                                uom: "USD"
                        ]
                ],
                payments: [
                        [
                                accountNumber: "1111222233334444",
                                balances: [
                                        [
                                                amount: 1000,
                                                type: "refundable",
                                                uom: "USD"
                                        ],
                                        [
                                                amount: 1000,
                                                type: "nonRefundable",
                                                uom: "USD"
                                        ]
                                ],
                                count: 1,
                                expirationDate: "2027-10-01T23:59:59.999-05:00",
                                type: "P_CREDIT_CARD"
                        ]
                ],
                tickets: [
                        [
                                createDate: "2023-02-01T23:59:59.999-05:00",
                                id: "{{ticket}}"
                        ]
                ],
                travelers: [
                        [
                                emailAddress: "cs2autotest@wnco.com",
                                firstName: "Yptertxz",
                                frequentFlyerId: "{{FFID}}",
                                lastName: "Kosvzirb",
                                mailingAddressState: "MI"
                        ]
                ],
                type: "RTF"
        )
    }
    response {
        status 200
        body(
                assetNumber: "1000002116153236",
                availableBalance: 2000,
                expirationDate: "2040-12-31T23:59:59.999-06:00",
                id: "dc582591-4b05-4ca4-9bb0-4d9dab56581d",
                status: "OPEN",
                type: "RTF",
                uom: "USD"
        )
        headers {
            header('Content-Type', 'application/json')
        }
    }
}
