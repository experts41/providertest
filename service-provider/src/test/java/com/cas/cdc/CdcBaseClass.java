package com.cas.cdc;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

@SpringBootTest(classes = ProviderApplication.class)
public abstract class CdcBaseClass {
    @Autowired
    Controller restController;

    @MockBean
    RTFService orderService;

    @BeforeEach
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(restController);

        Mockito.when(orderService.getAllOrders())
                .thenReturn(List.of(
                        new RTFService.Order("1000002116153236", 2000.00, "2040-12-31T23:59:59.999-06:00",
                                "dc582591-4b05-4ca4-9bb0-4d9dab56581d", "OPEN", "RTF", "USD")
                ));
    }
}
