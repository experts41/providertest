package com.cas.cdc;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RTFService {

    private final List<Order> orders = new ArrayList<>();

   public RTFService() {
        // Initialize orders
        orders.add(new Order("1000002116153236", 2000.00, "2040-12-31T23:59:59.999-06:00",
                "dc582591-4b05-4ca4-9bb0-4d9dab56581d", "OPEN", "RTF", "USD"));
    }

   public List<Order> getAllOrders() {
        return orders;
    }

    @Data
    @AllArgsConstructor
    static class Order {

        private String assetNumber;

        private double availableBalance;

        private String expirationDate;

        private String id;

        private String status;

        private String type;

        private String uom;
    }
}
