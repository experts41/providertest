package com.cas.cdc;

import com.cas.cdc.RTFService.Order;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class Controller {

    private final RTFService orderService;

    public Controller(RTFService orderService) {
        this.orderService = orderService;
    }


    @GetMapping(value = "/assets", produces = "application/json")
    public ResponseEntity<List<Map<String, Object>>> getAllOrders() {
        List<Order> orders = orderService.getAllOrders();
        List<Map<String, Object>> response = orders.stream()
                .map(order -> Map.of(
                        "assetNumber", (Object) order.getAssetNumber(),
                        "availableBalance", (Object) order.getAvailableBalance(),
                        "expirationDate", (Object) order.getExpirationDate(),
                        "id", (Object) order.getId(),
                        "status", (Object) order.getStatus(),
                        "type", (Object) order.getType(),
                        "uom", (Object) order.getUom()
                ))
                .collect(Collectors.toList());
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(response);
    }


}
