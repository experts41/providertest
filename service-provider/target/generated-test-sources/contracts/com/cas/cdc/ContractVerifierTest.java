package com.cas.cdc;

import com.cas.cdc.CdcBaseClass;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification;
import io.restassured.response.ResponseOptions;

import static org.springframework.cloud.contract.verifier.assertion.SpringCloudContractAssertions.assertThat;
import static org.springframework.cloud.contract.verifier.util.ContractVerifierUtil.*;
import static com.toomuchcoding.jsonassert.JsonAssertion.assertThatJson;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;

@SuppressWarnings("rawtypes")
public class ContractVerifierTest extends CdcBaseClass {

	@Test
	public void validate_create_rtf_contract() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.header("callingService", "87878787")
					.header("transactionId", "CAS.CAM1351450074")
					.header("userId", "x306486")
					.header("userLocation", "HDQ")
					.body("{\"activateDate\":\"2023-12-31T23:59:59.999-05:00\",\"bookings\":[{\"createDate\":\"2023-01-01T23:59:59.999-05:00\",\"id\":\"\",\"sourceOfSale\":{\"originatorTypeCode\":\"A\",\"reservationSystemCode\":\"1A\"},\"type\":\"air\"}],\"corporations\":[{\"arcNumber\":\"2212333\",\"company\":{\"id\":\"\",\"name\":\"Southwest Airlines\"},\"osi\":\"some value\",\"pcc\":\"AMSX1234P\",\"tourCode\":\"403SH\"}],\"coupons\":[{\"arrivalAirportCode\":\"DAL\",\"departureAirportCode\":\"HOU\",\"departureDate\":\"2023-03-01T23:59:59.999-05:00\",\"fareBasisCode\":\"FZMUHNR\",\"fareFamily\":\"BUS\",\"flightNumber\":\"1234\",\"marketingCarrierCode\":\"WN\",\"number\":1}],\"expirationDate\":\"2040-12-31T23:59:59.999-05:00\",\"issuanceContext\":{\"isReissued\":false,\"location\":\"RrzHLn1ud7\",\"reason\":\"Ticket Refund\",\"reasonDetails\":\"Additional details\",\"sendFulfillmentEmail\":false},\"issuedBalances\":[{\"amount\":1000,\"expirationDate\":\"2024-01-01T23:59:59.999-05:00\",\"type\":\"refundable\",\"uom\":\"USD\"},{\"amount\":1000,\"expirationDate\":\"2024-01-01T23:59:59.999-05:00\",\"type\":\"nonRefundable\",\"uom\":\"USD\"},{\"amount\":1000,\"expirationDate\":\"2024-01-01T23:59:59.999-05:00\",\"type\":\"transferable\",\"uom\":\"USD\"},{\"amount\":1000,\"expirationDate\":\"2024-01-01T23:59:59.999-05:00\",\"type\":\"nonTransferable\",\"uom\":\"USD\"}],\"payments\":[{\"accountNumber\":\"1111222233334444\",\"balances\":[{\"amount\":1000,\"type\":\"refundable\",\"uom\":\"USD\"},{\"amount\":1000,\"type\":\"nonRefundable\",\"uom\":\"USD\"}],\"count\":1,\"expirationDate\":\"2027-10-01T23:59:59.999-05:00\",\"type\":\"P_CREDIT_CARD\"}],\"tickets\":[{\"createDate\":\"2023-02-01T23:59:59.999-05:00\",\"id\":\"\"}],\"travelers\":[{\"emailAddress\":\"cs2autotest@wnco.com\",\"firstName\":\"Yptertxz\",\"frequentFlyerId\":\"\",\"lastName\":\"Kosvzirb\",\"mailingAddressState\":\"MI\"}],\"type\":\"RTF\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.get("/assets");

		// then:
			assertThat(response.statusCode()).isEqualTo(200);
			assertThat(response.header("Content-Type")).isEqualTo("application/json");

		// and:
			DocumentContext parsedJson = JsonPath.parse(response.getBody().asString());
			assertThatJson(parsedJson).field("['assetNumber']").isEqualTo("1000002116153236");
			assertThatJson(parsedJson).field("['availableBalance']").isEqualTo(2000);
			assertThatJson(parsedJson).field("['expirationDate']").isEqualTo("2040-12-31T23:59:59.999-06:00");
			assertThatJson(parsedJson).field("['id']").isEqualTo("dc582591-4b05-4ca4-9bb0-4d9dab56581d");
			assertThatJson(parsedJson).field("['status']").isEqualTo("OPEN");
			assertThatJson(parsedJson).field("['type']").isEqualTo("RTF");
			assertThatJson(parsedJson).field("['uom']").isEqualTo("USD");
	}

}
