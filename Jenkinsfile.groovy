pipeline {
    agent any
    stages {
        stage('build') {
            steps {
                sh "mvn clean install -f service-provider/pom.xml"
            }
            post {
                success {
                    archiveArtifacts(artifacts: 'service-provider/target/')
                }
            }
        }
        stage('deploy-stubs') {
            steps {
                sh "mkdir -p ./stubs"
                sh "cp /builds/experts41/providertest/service-provider/target/service-provider-0.0.1-SNAPSHOT-stubs.jar ./stubs"
            }
            post {
                success {
                    archiveArtifacts(artifacts: './stubs', expireIn: '1 week')
                }
            }
            when {
                manual
            }
        }
        stage('test') {
            steps {
                sh "mvn test -f service-provider/pom.xml"
            }
        }
        stage('run') {
            steps {
                sh "mvn exec:java -Dexec.mainClass='com.cas.cdc.ProviderApplication' -f service-provider/pom.xml"
            }
        }
    }
}
